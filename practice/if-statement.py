'''
1. A un profesor le pagan según sus horas y una tarifa
de pago por horas. Si la cantidad de horas trabajadas
es mayor a 40 horas, la tarifa se incrementa en un 50%
para las horas extras. Calcular el salario de 7 profesores
dadas las horas trabajadas y la tarifa. Utilice el ciclo
while para resolver el algoritmo.
'''

def calcule_pay(professor, time, rate):
	for i in range(7):
		time_tmp = time[i]
		rate_tmp = rate[i]
		print('\n')

		if time_tmp <= 40:
			pay = time_tmp * rate_tmp

			print('{0} su salario es de {1}'.format(professor[i], pay))
		else:
			extra = time_tmp - 40
			extra_pay = (extra * rate_tmp * 1.5) 
			pay = 40 * rate_tmp + extra_pay

			print('{0} su salario es de {1} y tuvo un incremento de {2}'.format(professor[i], pay, extra_pay))




import os
import macarena as mac

opt = 1
while (opt == 1):
	os.system('clear')
	print('\tXY SCHOOL - TEACHERS SITE\n')
	t_names = list()
	t_hours = list()
	t_pay = list()

	teacher = 1
	while (teacher <= 7):
		my_bool = True
		while (my_bool):
			name = input('\n{0}. Ingrese el nombre del profesor: '.format(teacher))
			my_bool, (name, ) = mac.input_name(name)
		
		my_bool = True
		while (my_bool):
			hours = input('Ingrese las horas trabajadas: ')
			pay_h = input('Ingrese la tarifa: ')
			my_bool, (pay_h, hours) = mac.casting_InLoop(int, 'p', pay_h, hours)

		teacher += 1

		t_names.append(name)
		t_hours.append(hours)
		t_pay.append(pay_h)

	calcule_pay(t_names, t_hours, t_pay)

	# print(t_names, t_hours, t_pay)


	opt = mac.repeat_menu()