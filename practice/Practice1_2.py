'''
8.4 Open the file romeo.txt and read it line by line. For each line,
split the line into a list of words using the split() method. The
program should build a list of words. For each word on each line
check to see if the word is already in the list and if not append
it to the list. When the program completes, sort and print the
resulting words in alphabetical order.
'''
import macarena as mac
import getpass

select = 1
while (select == 1):
    
    mac.clear_console()
    
    this_loop =  True
    while (this_loop):
        
        file_name = input('Enter file name: ')
        if (len(file_name) < 1):
            file_name = 'romeo.txt'
        if ('.txt' not in file_name):
            file_name += '.txt'
            
        try:
            file_handler = open(file_name)
            this_loop = False
        except:
            print('file name not found.')
            getpass.getpass('')
    
    words_list = list()        
    for lines in file_handler:
        lines = lines.rstrip()
        sentences = lines.split()
            
        for words in sentences:
            if words not in words_list:
                words_list.append(words)
    
    words_list.sort()
    print(words_list)

    select = mac.repeat_menu()