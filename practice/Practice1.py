'''
8.4 Open the file romeo.txt and read it line by line. For each line,
split the line into a list of words using the split() method. The
program should build a list of words. For each word on each line
check to see if the word is already in the list and if not append
it to the list. When the program completes, sort and print the
resulting words in alphabetical order.
'''

import subprocess as sub
import platform
import getpass

op_s = platform.system()

this_loop = True
while (this_loop):
    if (op_s == 'Windows'):
        sub.run('cls')
    else:
        sub.run('clear')
    
    file_name = input('Enter file name: ')
    if len(file_name) < 1:
        file_name = 'romeo.txt'
    try:
        if '.txt' not in file_name:
            file_name += '.txt'
        file_hand = open(file_name)
        this_loop = False
    except:
        print('file not found')
        getpass.getpass('')
        
words_list = list()
for line in file_hand:
    line = line.rstrip()
    words = line.split()
    
    for each in words:
        if each not in words_list:
            words_list.append(each)
            
words_list.sort()
print(words_list)