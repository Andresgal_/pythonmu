'''
8.5 Open the file mbox-short.txt and read it line by line.
When you find a line that starts with 'From ' like the
following line:

    From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008

You will parse the From line using split() and print out
the second word in the line (i.e. the entire address of the
person who sent the message). Then print out a count at the end.

Hint: make sure not to include the lines that start with 'From:'.
'''

import getpass as getp
import macarena as mac

options = 1
while (options == 1):
    mac.clear_console()
    
    loop_program =  True
    while (loop_program):
        file_name =  input('Enter file name: ')
        if (len(file_name) < 1):
            file_name = 'mbox-short.txt'
        if ('.txt'not in file_name):
            file_name += '.txt'
            
        try:
            file_hand =  open(file_name)
            loop_program = False
        except:
            print('file name not found.')
            getp.getpass('')
    
    count = 0            
    for lines in file_hand:
        lines = lines.rstrip()
        
        if (lines.startswith('From ')):
            count += 1
            emails = lines.split()
            print(emails[1]) 
                       
    print('\nEmail count:', count)
    options =  mac.repeat_menu()