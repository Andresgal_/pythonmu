'''
8.5 Open the file mbox-short.txt and read it line by line.
When you find a line that starts with 'From ' like the
following line:

    From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008

You will parse the From line using split() and print out
the second word in the line (i.e. the entire address of the
person who sent the message). Then print out a count at the end.

Hint: make sure not to include the lines that start with 'From:'.
'''

import os
import getpass

this_loop = True
while (this_loop):
    os.system('clear')
    
    file_name = input('Enter file name: ')
    if len(file_name) < 1:
        file_name = 'mbox-short.txt'
    try:
        file_handler = open(file_name)
        this_loop = False
    except:
        print('file name not found')
        getpass.getpass('')
        
count = 0
for line in file_handler:
    line = line.rstrip()
    if line.startswith('From '):
        count += 1
        email = line.split()
        print(email[1])
        
print('\nEmail count: {0}'.format(count))