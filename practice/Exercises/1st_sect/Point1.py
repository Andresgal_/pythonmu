'''En un almacén se hace un 20% de descuento a los clientes
cuya compra supere los $100.000, si es mayor o igual a $400.000
por cada 100 adicional restarle un 5% hasta 30% al 20% descontado.
¿Cuál será la cantidad que pagará una persona por su compra?'''

def computation_pay(value):
    if (value <= 100.000):
        paying = value
    elif (value < 400.00):
        paying = value * 0.8
    else:
        each_five = (value - 400.000) // 100
        value *= 0.8
        each_five *= 5
        if each_five > 30:
            each_five = 30
        discount = (100 - each_five) / 100

        paying = value * discount
    return round(paying)

import os
from module import macarena

option = 1
while (option == 1):
    main_menu = True
    while (main_menu):
        os.system('clear')
        ini_val = input('Enter the price of your buy: ')
        
        main_menu, ini_val = macarena.casting_input_InLoop(float, 'p', ini_val)

    final_val = computation_pay(ini_val)

    print('The price of your bought is ${0}.'.format(final_val))

    option = macarena.repeat_menu()
