'''Hacer un algoritmo que calcule el total a pagar por la compra
de camisas. Sí se compran tres camisas o más se aplica un descuento
del 20% sobre el total de la compra y sí son menos de tres camisas
concederán un descuento del 10%'''
def pay_calculus(quantity_s, price):
    if quantity_s < 3:
        total_p = quantity_s * price * 0.8
    else:
        total_p = quantity_s * price * 0.9

    return total_p




import os
from module import macarena

opt = 1
while(opt == 1):
    menu = True
    while(menu):
        os.system('clear')
        q_shirts = input('Enter shirts\' quantity: ')
        v_shirts = input('Enter shirts\' value: ')
        menu, (q_shirts, v_shirts) = macarena.casting_InLoop(float, 'p', q_shirts, v_shirts)
    
    t_pay = pay_calculus(q_shirts, v_shirts)
    print('\nEl total a pagar es de {0}'.format(t_pay))

    opt = macarena.repeat_menu()