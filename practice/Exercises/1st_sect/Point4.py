'''Leer 2 números; Sí son iguales que los multiplique, sí el
primero es mayor que el segundo los reste y sino que los sume.
'''      


def crazy_cal(n1, n2):
    if n1 > n2:
        r = n1 - n2
    elif n1 < n2:
        r = n1 + n2
    else:
        r = n1 * n2

    return r

import os
from module import macarena as mac

r_menu = 1
while (r_menu == 1):
    os.system('clear')
    print('CRAZY CALCULES.\n')

    this_loop = True
    while(this_loop):
        num1 = input('Ingrese el primer número: ')
        num2 = input('Ingrese el segundo número: ')

        this_loop, (num1, num2) = mac.casting_InLoop(float,'n', num1, num2)

    result = crazy_cal(num1, num2)
    print(result)

    r_menu = mac.repeat_menu()