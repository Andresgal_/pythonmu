# Macarena 1.0.1
# First library

import getpass
import os
from colorama import Fore



# List of errors
def error_msg(error_type):

    error_list = list()
    error_list = ['Error List Working.',
    'Error 101. Only input numbers.',
    'Error 102. Only input positive numbers.',
    'Error 103. Option out of range.',
    'Error 104. Variable type not suported.',
    'Error 105. Only input negative numbers']

    if error_type == 0:
        print(Fore.GREEN + error_list[error_type])
    else:
        print(Fore.RED + error_list[error_type])
    print(Fore.WHITE)



# Syntax errors
def syntax_msg(error_type):   
    syntax_l = list()
    syntax_l = ['Syntax List Working.',
    'In function "casting_InLoop()".\n'
    '- Put an argument of "n" for allow negatives values.\n'
    '- Put an argument of "p" for allow only positives values.\n'
    '- Put an argument of "-n" for allow only negatives values.']

    if error_type == 0:
        print(Fore.GREEN + syntax_l[error_type])
    else:
        print('\n' + Fore.RED + '\tMacarena Syntax Error :/\n')
        print(Fore.WHITE + "--------------------------------------------")
        print(syntax_msg[error_type])
    print(Fore.WHITE)




#Repeating menu
def repeat_menu():
    err_count = 0
    repeat = True
    while (repeat):
        getpass.getpass('')
        if err_count >= 1:
            os.system('clear')
        
        print('\n\tSelect an option:\n')
        choice = input('[1] Repeat.            [2] EXIT. \n\n> ')
        try:
            choice = int(choice)
        except:
            repeat = True
            err_count += 1
            error_msg(1)
            continue
        
        if choice > 2 or choice < 1:
            error_msg(3)
            err_count += 1
        else: repeat = False

    return choice




#-------------------------------------------------------------
#CASTING

# Cast an argument with whatever type of variable
def casting_input(validation_arg, type_var):
    try:
        if type_var == int:
            validation_arg = float(validation_arg)
        validation_arg = type_var(validation_arg)
    except:
        if type_var == int or type_var == float:
            error_msg(1)
        else:
            error_msg(4)
        quit()
    
    return validation_arg




# Cast an argument in a loop
def casting_input_InLoop(type_var, cartesian, value_arg):
    m_menu = False
    try:
        if type_var == int:
            value_arg = float(value_arg)
        value_arg = type_var(value_arg)
    except:
        if type_var == int or type_var == float:
            error_msg(1)
        else:
            error_msg(4)
        getpass.getpass('')
        m_menu = True
    
    if m_menu == False:
        if (cartesian == 'a' or cartesian == 'A'):
            pass
        elif (cartesian == 'p' or cartesian == 'P'):
            if value_arg < 0: 
                error_msg(2)
                getpass.getpass('')
                m_menu = True
        elif (cartesian == 'n' or cartesian == 'N'):
            if value_arg > 0:
                error_msg(5)
                getpass.getpass('')
                m_menu = True
        else:
            syntax_msg(1)

    return m_menu, value_arg




# Cast n lot of arguments in a loop
def casting_InLoop(type_var, cartesian, *args):
    values = list()
    m_menu = False
    count = len(args)
    mac_error = False

    try:
        for each_value in args:
            if type_var == int:
                each_value = float(each_value)
            new_type = type_var(each_value)
            values.append(new_type)
    except:
        if type_var == int or type_var == float:
            error_msg(1)
        else:
            error_msg(4)
        getpass.getpass('')
        m_menu = True
        values = []
        for i in range (count):
            values.append(0)

    if m_menu == False:
        for each_value in values:
            if (cartesian == 'a' or cartesian == 'A'):
                break
            elif (cartesian == 'p' or cartesian == 'P'):
                if each_value < 0: 
                    error_msg(2)
                    getpass.getpass('')
                    m_menu = True
            elif (cartesian == 'n' or cartesian == 'N'):
                if each_value > 0:
                    error_msg(5)
                    getpass.getpass('')
                    m_menu = True
            else:
                mac_error = True
        if mac_error == True:
            syntax_msg(1)

    return m_menu, values