'''
8.4 Open the file romeo.txt and read it line by line. For each line,
split the line into a list of words using the split() method. The
program should build a list of words. For each word on each line
check to see if the word is already in the list and if not append
it to the list. When the program completes, sort and print the
resulting words in alphabetical order.
'''

import os

thisloop = True
while (thisloop):
    os.system('clear')
    thisloop = False
    file_name = input('Enter file name: ')
    try:
        file_handled = open(file_name)
    except:
        print('file not found')
        thisloop = True
        input()

script_list = list()
for line in file_handled:
    line = line.rstrip()
    size = line.split()
    
    for words in size:
        if words not in script_list:
            script_list.append(words)

script_list.sort()
print(script_list)