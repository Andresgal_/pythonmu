counts =  dict()
names = ['Arthur', 'James', 'Chris', 'James', 'Chris', 'Chris', 'Loren']

for name in names:
    if name not in counts:
        counts[name] = 1
    else:
        counts[name] += 1
        
print(counts)
print()













easier = dict()
users = ['James', 'Arthur', 'Loren', 'Arthur', 'Loren', 'Chris', 'Loren']

for user in users:
    easier[user] = easier.get(user, 0) + 1
    
print(easier)
print()















counts = dict()
counts = {'James': 2, 'Arthur': 9, 'Loren':17}
for key in counts:
    print(key, counts[key])
    













# METHODS
"""
data = {'Chuck': 5, 'Alfred': 22, 'Jonas': 13}
print(list(data))
print()
k = data.keys()
print(k)
print()
v = data.values()
print(v)
print()
i = data.items()
print(i)

print()
t = tuple()
for j in i:
    t += j

print(t)
print(len(t))

print()
l = list()
for mk in i:
    l.append(mk)

print(l)
print(len(l))
"""









print()    
ddd = {'Chuck': 5, 'Alfred': 22, 'Jonas': 13}
for kkk, vvv in ddd.items():
    print(kkk, vvv)

print()    
print('**SORTING**')
l = list()
for mk in ddd:
    l.append(mk)
    
l.sort()
print(l)



print()
print('**SORT WITH VALUES**')

c = {'a': 4, 'b': 8, 'c': 1, 'd': 3, 'e': 6}
sort_tp = list()

print( sorted( [ (v,k) for k,v in c.items() ], reverse = True ) )

"""
for k, v in c.items():
    sort_tp.append((v, k))
    
print(sort_tp)
print()

sort_tp = sorted(sort_tp, reverse = True)
print(sort_tp)
print()
"""
    
    
    
    
    
    
    
    
    
    
    
    
    
print()
# COUNTING WORDS
file = ['James', 'Arthur', 'Loren', 'Arthur', 'Loren', 'Chris', 'Loren', 'Arthur', 'James', 'Chris', 'James', 'Chris', 'Chris', 'Loren', 'Loren', 'Chris']

counter = dict()
for words in file:
    counter[words] = counter.get(words, 0) + 1
    
LargeCount = None
LargeWord = None

for word, count in counter.items():
    if LargeCount is None or count > LargeCount:
        LargeCount = count
        LargeWord = word
    
print(LargeWord, LargeCount)

numbers = [5, 7, 3, 6, 4, 88, 10]
highest = max(numbers)
print()
print(highest)