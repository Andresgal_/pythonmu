"""
9.4 Write a program to read through the mbox-short.txt
and figure out who has sent the greatest number of mail
messages. The program looks for 'From ' lines and takes
the second word of those lines as the person who sent
the mail. The program creates a Python dictionary that
maps the sender's mail address to a count of the number
of times they appear in the file. After the dictionary
is produced, the program reads through the dictionary
using a maximum loop to find the most prolific committer.
"""

loop_menu = True
while (loop_menu):
    file_name =  input('Enter file name: ')
    if (len(file_name) < 1): file_name = 'mbox-short.txt'
    if ('.txt' not in file_name): file_name += '.txt'
        
    try:
        file_hand = open(file_name)
        loop_menu = False
    except:
        print('file not found.')
        
counts = dict()
for lines in file_hand:
    if lines.startswith('From '):
        email = lines.split()
        people = email[1]
        counts[people] = counts.get(people, 0) + 1
        
commonperson = None
commoncount = None
for person, count in counts.items():
    if commoncount is None or count > commoncount:
        commonperson = person
        commoncount = count
        
print (commonperson, commoncount)