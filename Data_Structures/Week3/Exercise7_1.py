'''
7.1 Write a program that prompts for a file name, then opens that
file and reads through the file, and print the contents of the file
in upper case. Use the file words.txt to produce the output below.
'''

file_name = input('Enter file name: ')
try:
    file_handled = open(file_name)
except:
    print("File doesn't exist or misspelling")
    quit()

for line in file_handled:
    line = line.rstrip()
    print(line.upper())