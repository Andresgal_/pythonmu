'''7.2 Write a program that prompts for a file name, then opens
that file and reads through the file, looking for lines of the form:

        X-DSPAM-Confidence:    0.8475

Count these lines and extract the floating point values from each of
the lines and compute the average of those values and produce an output
as shown below. Do not use the sum() function or a variable named sum
in your solution.'''

file_name = input('Enter file name: ')
try:
    file_handled = open(file_name)
except:
    print("File doesn't exist or misspelling")
    quit()

each_line = 0
addition = 0

for line in file_handled:
    if not line.startswith('X-DSPAM-Confidence:'): continue

    first = line.find(':')
    piece = line[first+1:]

    value = float(piece)

    addition += value
    each_line += 1

avrg = addition/each_line

print('Average spam confidence:', avrg)