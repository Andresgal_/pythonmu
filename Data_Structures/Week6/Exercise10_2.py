"""
10.2 Write a program to read through the mbox-short.txt
and figure out the distribution by hour of the day for
each of the messages. You can pull the hour out from the
'From ' line by finding the time and then splitting the
string a second time using a colon.

    From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008

Once you have accumulated the counts for each hour, print
out the counts, sorted by hour as shown below.
"""

loop_menu = True
while (loop_menu):
    file_name =  input('Enter file name: ')
    if (len(file_name) < 1): file_name = 'mbox-short.txt'
    if ('.txt' not in file_name): file_name += '.txt'
        
    try:
        file_hand = open(file_name)
        loop_menu = False
    except:
        print('file not found.')

counts =  dict()
for lines in file_hand:
    if lines.startswith('From '):
        emails = lines.split()
        parts = emails[5]
        
        times = parts.split(':')
        hours = times[0]
        counts[hours] = counts.get(hours, 0) + 1

hours_sorted = sorted( [ (k, v) for k, v in counts.items() ] )

for k, v in hours_sorted:
    print(k, v)