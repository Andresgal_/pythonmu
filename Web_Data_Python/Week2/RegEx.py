"""
^	        Matches the beginning of a line
$	        Matches the end of the line
.	        Matches any character
\s	        Matches whitespace
\S	        Matches any non-whitespace character
*	        Repeats a character zero or more times
*?	        Repeats a character zero or more times (non-greedy)
+	        Repeats a character one or more times
+?	        Repeats a character one or more times (non-greedy)
[aeiou]	    Matches a single character in the listed set
[^XYZ]	    Matches a single character not in the listed set
[a-z0-9]	The set of characters can include a range
(	        Indicates where string extraction is to start
)	        Indicates where string extraction is to end
"""


import re

x = 'My 2 favorites numbers are 19 and 42'
y = re.findall('[0-9]+', x)
print(y)

k = 'From: Using the: characters'
z = re.findall('^F.+:', k) # Greedy
print(z)
z = re.findall('^F.+?:', k) # Non-Greedy
print(z)
z = re.findall('^F.\S+:', k) # Ignore spaces

msg = 'From: stephen.marquard@uct.ac.za Sat Jan 5 09:14:53 2008'
email = re.findall('\S+@\S+', msg)
print('**Here**')
print(email)
email = re.findall('^From: (\S+@\S+)', msg)
print(email)
host = re.findall('@([^ ]*)', msg)
print(host)
host = re.findall('@(\S+)', msg)
print(host)

host = re.findall('From: .*@([^ ]*)', msg)
print(host)
host = re.findall('From: .*@(\S+)', msg)
print(host)
