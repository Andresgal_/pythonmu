#!/usr/bin/env python3

import re

file = 'regex_sum_780545.txt'
hand = open(file)

addition = 0
for lines in hand:
    lines = lines.rstrip()
    search = re.findall('[0-9]+', lines)
    
    for i in search:
        allnumbs = i.split()
        
        cast_int = int(i)
        addition += cast_int

print(addition)